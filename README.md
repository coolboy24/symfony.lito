
Symfony REST Edition + WebApp
========================

1. a) Setting unused host for your App
------------------------------------

In httpd.conf file set :

Listen 8080

b) Configure your httpd-vhosts.conf file in apache\conf\extra folder:
----------------------------------------------------------------------

    <VirtualHost *:8080>

    ServerAdmin webmaster@symfony.lito

    DocumentRoot "WebRootDirectory/symfony.rest/web"

    ServerName symfony.lito

    ErrorLog "logs/symfony.lito-error.log"

    CustomLog "logs/symfony.lito-access.log" common

    </VirtualHost>



c) Installing the REST Edition
----------------------------------

### Download an Archive File

To quickly test Symfony, you can also download an [archive][3] of the Standard
Edition and unpack it somewhere under your web server root directory.

If you downloaded an archive "without vendors", you also need to install all
the necessary dependencies. Download composer (see above) and run the
following command:

    php composer.phar install

d) Checking your System Configuration
-------------------------------------

Before starting coding, make sure that your local system is properly
configured for Symfony.

Execute the `check.php` script from the command line:

    php app/check.php

Access the `config.php` script from a browser:

    http://localhost/path/to/symfony/app/web/config.php

If you get any warnings or recommendations, fix them before moving on.


e) Deleting Example Bundle from Symfony.
-------------------------------

A default bundle, `AcmeDemoBundle`, shows you Symfony2 in action. You can remove it by following these steps:

  * delete the `src/Acme` directory;

  * remove the routing entries referencing AcmeBundle in
    `app/config/routing_dev.yml`;

  * remove the AcmeBundle from the registered bundles in `app/AppKernel.php`;

  * remove the `web/bundles/acmedemo` directory;

  * remove the `security.providers`, `security.firewalls.login` and
    `security.firewalls.secured_area` entries in the `security.yml` file or
    tweak the security configuration to fit your needs.



2, 3) Creating Bundles ApiBundle, FrontendBundle and moving Web App to this Bundle.
-----------------------------------------------------------------------------------
All Web App files located in src\API\FrontendBundle\Resources\public folder.

Run `npm install --save`


Also you need to run    `php app/console assets:install --symlink`    in command line.



4) For loading Web App by Default routing I used this routing:
--------------------------------------------------------------
```sh
api_frontend_homepage:

    path:     /

    defaults: { _controller: APIFrontendBundle:Default:index }`
```


5) To disable Symfony 404 error page and handle this error by Web app I changed showAction() in Exception Controller in Rest Bundle.
-------------------------------------------------------------------------------------------------------------------------------------


6)I created API Content responce this way:
-------------------------------------------
   a) Added this routing in routing_dev.yml

    demo_response:

    resource: "@APIApiBundle/Controller/DefaultController.php"
    prefix: /api
    type:     rest

   b) In DefaulController (ApiBundle) added this function:

     /**
      * Get a dummy text.
      *
      * @ApiDoc(
      *   resource = true,
      *   statusCodes = {
      *     200 = "Returned when successful",
      *     404 = "Returned when the note is not found"
      *   }
      * )
      *
      *
      * @param Request $request the request object
      *
      * @return array
      */
     public function getContentsAction(Request $request)
     {
       $response = 'Hello world!';
       return new JsonResponse(array($response));
     }

   We have response using contents.json, which used in a default Ajax call from the web app.
