/** @jsx React.DOM */

var React = require("react");

var Gallery = React.createClass({
	getInitialState: function() {
		return {
			showIndex: 0
		}
	},
	next: function(event) {
		if (this.state.showIndex + 1 === this.props.images.length) {
			this.setState({showIndex: 0})
		} else {
			this.setState({showIndex: this.state.showIndex + 1})
		}
	},
	previous: function(event) {
		if (this.state.showIndex - 1 === - 1) {
			this.setState({showIndex: this.props.images.length - 1})
		} else {
			this.setState({showIndex: this.state.showIndex - 1})
		}
	},
	render: function() {
		var self = this;
		var images = [];
		_.each(self.props.images, function(image, index) {
			var style = {};
			if (index === self.state.showIndex) {
				style.display = 'block';
			}
			images.push(
				<div className="img" style={style}>
					<img src={image.src} />
					<div className="title">{image.title}</div>
				</div>
				);
		});
		return (
			<div className="gallery">
				<div className="images">
					{images}
				</div>
				<div className="next clicker" onClick={this.next}>
				</div>
				<div className="previous clicker" onClick={this.previous}>
				</div>
			</div>
		);
	}
});

module.exports = Gallery;