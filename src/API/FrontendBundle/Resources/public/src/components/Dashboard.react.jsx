/** @jsx React.DOM */

var React = require("react");

var AuthenticatedRouteMixin = require('../mixins/AuthenticatedRouteMixin');

var AuthClient = require('../utils/AuthClient');

var Dashboard = React.createClass({
  mixins: [AuthenticatedRouteMixin],

  render: function() {
    var token = AuthClient.getToken();
    return (
      <div>
        <h1>Dashboard</h1>
        <p>You made it!</p>
        <p>{token}</p>
      </div>
    );
  }
});

module.exports = Dashboard;