/** @jsx React.DOM */

var React = require("react");

var Env = require("./../utils/Env");

var LanguageSelector = React.createClass({
        getInitialState: function() {
            return {
                newCode: this.getOtherLanguages()
            }
        },

        getOtherLanguages: function () {
            return _.filter(Env.getAvailableLanguages(), function(code) {
                return code !== Env.getLocale();
            })[0];
        },

        changeLanguage: function () {
            Env.setLocale(this.state.newCode);
            window.location.reload();
        },

        render: function () {

            return (
                <div className="clicker" onClick={this.changeLanguage}>{this.state.newCode}</div>
                );
        }
});

module.exports = LanguageSelector;