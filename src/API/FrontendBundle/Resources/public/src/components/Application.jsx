/** @jsx React.DOM */

var React = require("react"),
    Fluxxor = require("fluxxor");
/*
var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;
*/
 
var Router = require('react-router'),
    Navigation = Router.Navigation,
    Link = Router.Link;
  
var Header = require('./Header.jsx'),
    Footer = require('./Footer.jsx');


var Application = React.createClass({
    mixins: [Navigation],
    componentDidMount: function() {
      
    },
    render: function() {
      return (
        <div id="container" className="container">
          <Header />
          {this.props.activeRouteHandler()}
          <Footer />
        </div>
      );
  },

/*  componentDidMount: function() {
    this.getFlux().actions.buzz.loadBuzz();
  },

  handleSuggestedWordChange: function(e) {
    this.setState({suggestBuzzword: e.target.value});
  },

  handleSubmitForm: function(e) {
    e.preventDefault();
    if (this.state.suggestBuzzword.trim()) {
      this.getFlux().actions.buzz.addBuzz(this.state.suggestBuzzword);
      this.setState({suggestBuzzword: ""});
    }
  }*/
});

module.exports = Application;