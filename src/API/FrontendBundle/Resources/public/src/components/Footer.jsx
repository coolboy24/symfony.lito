/** @jsx React.DOM */

var React = require("react");
var Router = require('react-router'),
    Navigation = Router.Navigation,
    Link = Router.Link;

var Footer = React.createClass({

    render: function () {
            return (
                

                <footer className="footer container clearfix center-block bs-docs-footer-links muted">
                    <div className="navbar-footer">
                      <div className="container">
                        <p className="text-muted navbar pull-left">
                          <span>© 2014 Lito</span>
                        </p>
                      </div>
                    </div>

                </footer>
            );
    }
});

module.exports = Footer