
// globals
window.React = require("react");

// bootstrap
var Router = require('./router.jsx');

React.renderComponent(Router, document.body);
