var Fluxxor = require("fluxxor");

var BuzzConstants = require('../constants/BuzzConstants');

var BuzzwordStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.adding = false;
    this.error = null;
    this.words = [];
    this.loadingWords = {};

    this.bindActions(
      BuzzConstants.LOAD_BUZZ, this.onLoadBuzz,
      BuzzConstants.LOAD_BUZZ_SUCCESS, this.onLoadBuzzSuccess,
      BuzzConstants.LOAD_BUZZ_FAIL, this.onLoadBuzzFail,
      BuzzConstants.ADD_BUZZ, this.onAddBuzz,
      BuzzConstants.ADD_BUZZ_SUCCESS, this.onAddBuzzSuccess,
      BuzzConstants.ADD_BUZZ_FAIL, this.onAddBuzzFail
    );
  },

  onLoadBuzz: function() {
    this.loading = true;
    this.words = [];
    this.error = null;
    this.emit("change");
  },

  onLoadBuzzSuccess: function(payload) {
    this.loading = false;
    this.words = payload.words.map(function(word) {
      return {word: word, status: "OK"};
    });
    this.emit("change");
  },

  onLoadBuzzFail: function(payload) {
    this.loading = false;
    this.error = payload.error;
    this.emit("change");
  },

  onAddBuzz: function(payload) {
    var word = {word: payload.word, status: "ADDING"};
    this.words.push(word);
    this.loadingWords[payload.id] = word;
    this.emit("change");
  },

  onAddBuzzSuccess: function(payload) {
    this.loadingWords[payload.id].status = "OK";
    delete this.loadingWords[payload.id];
    this.emit("change");
  },

  onAddBuzzFail: function(payload) {
    this.loadingWords[payload.id].status = "ERROR";
    this.loadingWords[payload.id].error = payload.error;
    delete this.loadingWords[payload.id];
    this.emit("change");
  }
});

module.exports = BuzzwordStore;