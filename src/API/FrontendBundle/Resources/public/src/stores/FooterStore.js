var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

var FooterStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = false;
    this.leftColumnHTML = false;
    this.rightColumnHTML = false;
    this.copyTextHTML = false;

    this.bindActions(
      ContentConstants.LOAD_CONTENT_FOOTER, this.onLoad,
      ContentConstants.LOAD_CONTENT_FOOTER_SUCCESS, this.onLoadSuccess,
      ContentConstants.LOAD_CONTENT_FOOTER_FAIL, this.onLoadFail
    );
  },

  onLoad: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadSuccess: function(data) {
    this.loading = false;
    this.leftColumnHTML = data.colPos2.bodytext;
    this.rightColumnHTML = data.colPos1.bodytext;
    this.copyTextHTML = data.colPos3.bodytext;

    this.emit("change");
  },

  onLoadFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },
});

module.exports = FooterStore;