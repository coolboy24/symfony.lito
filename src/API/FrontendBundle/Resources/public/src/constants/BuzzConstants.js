var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
  LOAD_BUZZ: null,
  LOAD_BUZZ_SUCCESS: null,
  LOAD_BUZZ_FAIL: null,

  ADD_BUZZ: null,
  ADD_BUZZ_SUCCESS: null,
  ADD_BUZZ_FAIL: null
});