/** @jsx React.DOM */

var Fluxxor = require("fluxxor");

var Router = require('react-router'),
    Route = Router.Route,
    Routes = Router.Routes,
    Redirect = Router.Redirect,
    DefaultRoute = Router.DefaultRoute,
    NotFoundRoute = Router.NotFoundRoute;

var flux = new Fluxxor.Flux();

var Application = require('./components/Application.jsx'),
    Index = require('./pages/Index.jsx');

var Hello = React.createClass({
  render: function() {
    return <h1>Hello</h1>;
  }
});


var World = React.createClass({
  render: function() {
    return <h1>World</h1>;
  }
});

var Login = React.createClass({
  render: function() {
    return <h1>Login</h1>;
  }
});

var Impressum = React.createClass({
  render: function() {
    return <h1>Impressum</h1>;
  }
});

var Router = (
  <Routes location="history">
    <Route name="dashboard" path="/" handler={Application}>
      <DefaultRoute name="start" handler={Index} flux={flux} />
      <Route name="hello" path="hello" handler={Hello}/>
      <Route name="world" path="world" handler={World}/>
      <Route name="login" path="login" handler={Login} flux={flux}/>
      <Route name="impressum" path="impressum" handler={Impressum} flux={flux}/>
      <NotFoundRoute handler={Index}/>
    </Route>
  </Routes>
);

module.exports = Router;