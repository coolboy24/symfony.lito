<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Content API example</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('bundles/apifrontend/build/css/bootstrap.css') ?>">
</head>

<body>
    <!--[if lt IE 8]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div id="container" class="container" data-reactid=".xvbnp977r4" data-react-checksum="1293986753">
        <div class="cms clearfix" data-reactid=".xvbnp977r4.0">
            <div class="jumbotron" data-reactid=".xvbnp977r4.0.0">
                <div class="container text-center" data-reactid=".xvbnp977r4.0.0.0">
                    <h1 data-reactid=".xvbnp977r4.0.0.0.0">React / Fluxxor / SF</h1>
                    <p data-reactid=".xvbnp977r4.0.0.0.1">Complex web apps made easy</p>
                </div>
            </div>
            <h3 data-reactid=".xvbnp977r4.0.1">Content API example</h3><span data-reactid=".xvbnp977r4.0.2">Waiting for response...</span>
        </div>
    </div>
    <script src="<?php echo $view['assets']->getUrl('bundles/apifrontend/build/app.js') ?>"></script>
    <!-- Google Analytics -->
    <script>
    </script>
</body>

</html>
