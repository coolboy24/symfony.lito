# Integrate a web app into the Symfony framework

### Background
We build dynamic web applications based on javascripts frameworks (backbone.js, angular.js etc). In this case this is a standalone starter kit from React / Fluxxor. As use to be for any single page application it communicates with backend using a REST API. 

### Main goals
* Since the main consumer is a web app, we use Symfony REST Edition in the backend. 
* The web app is runned on the same server as the backend and is a part of Symfony project.

### Tasks (PART 1)
* Basic Symfony REST Edition setup. Use build-in server from Symfony to work on local (i.e. http://127.0.0.1:8000)
* Create Api bundle for any stuff related REST. Every API should be available only under http://127.0.0.1:8000/api address.
* Move the web app into a symfony project, for example as a separate Frontend bundle.
* By default the app should be initially loaded from the default routing (http://127.0.0.1:8000). Therefore make some adjustments in config/routing/template etc.
* Disable Symfony 404 page for web requests. So if user tries to open a page, which is unknown for the backend (i.e. http://127.0.0.1:8000/world etc), it should be handled by web app.
* Create Content API to return a dummy text in the response. Use a default Ajax call from the web app as the specification. 

### A few tips for getting started
* Clone the repo
* Install node.js, npm as preconditions to work with the web app
* Install gulp for rebuilding the project and node packages:
```shell
$ cd webapp
$ npm install --save                  # Install Gulp, Node.js components listed in ./package.json
```
* To run the app as a standalone, simply run gulp server from the project folder
```shell
$ gulp serve
```
* Once the web app is migrated to Symfony project, you need to rebuild files from **build** folder:
```shell
$ gulp build                    # or, `gulp build --release`
```
or watch and rebuild on any change in the wep app folder 
```shell
$ gulp watch                    # or, `gulp build --release`
```
* Get Symfony REST Edition from [here](https://github.com/gimler/symfony-rest-edition.git).
