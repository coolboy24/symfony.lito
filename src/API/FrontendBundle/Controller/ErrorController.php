<?php

namespace API\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ErrorController extends Controller
{
  public function showAction()
  {
    return $this->render('APIFrontendBundle:Default:404.html.php');
  }
}

