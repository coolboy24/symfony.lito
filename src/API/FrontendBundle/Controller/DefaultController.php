<?php

namespace API\FrontendBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\ExceptionController;

class DefaultController extends FOSRestController
{
  public function indexAction()
  {
    return $this->render('APIFrontendBundle:Default:index.html.php');
  }
}

