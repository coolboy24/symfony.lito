<?php

namespace API\ApiBundle\Controller;

use FOS\RestBundle\Util\Codes;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\RouteRedirectView;
use Symfony\Component\HttpFoundation\JsonResponse;

use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends FOSRestController
{
  /**
   * Get a dummy text.
   *
   * @ApiDoc(
   *   resource = true,
   *   statusCodes = {
   *     200 = "Returned when successful",
   *     404 = "Returned when the note is not found"
   *   }
   * )
   *
   *
   * @param Request $request the request object
   *
   * @return array
   */
  public function getContentsAction(Request $request)
  {
    $response = 'Hello world!';
    return new JsonResponse(array($response));
  }
}